const mongoose = require('mongoose')
const Schema = mongoose.Schema
const usersSchema = new Schema({
  login: String,
  password: String,
  status: Boolean
})

const Users = mongoose.model('admins', usersSchema)

Users.find(function (err, usersPro) {
  if (err) return console.error(err)
  console.log(usersPro)
})
module.exports = mongoose.model('admins', usersSchema)